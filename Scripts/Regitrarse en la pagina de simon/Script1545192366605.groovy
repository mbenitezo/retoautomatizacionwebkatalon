import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.stringtemplate.v4.compiler.STParser.element_return
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import java.util.List
import personalize.Utils
import personalize.Esperar
import personalize.Seleccionar
import personalize.MoverPagina

System.setProperty("webdriver.chrome.driver", "//Users//usuario//Documents//Compartidos//simon//Drivers//chromedriver")

cantidad_registros = findTestData("DataForm").getRowNumbers()

for(int i=1; i<=cantidad_registros; i++){
WebDriver navegador = new ChromeDriver()
navegador.get("https://simon.inder.gov.co/registro")
navegador.findElement(By.id("s2id_tipopersona")).click()
navegador.findElement(By.id("s2id_autogen15_search")).sendKeys(findTestData("DataForm").getValue("tipo_cliente",i) + Keys.ENTER)
Esperar.LosSiguientes(3)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
navegador.findElement(By.id("select2-chosen-16")).click()
navegador.findElement(By.id("s2id_autogen16_search")).sendKeys(findTestData("DataForm").getValue("tipo_documento",i) + Keys.ENTER)
Esperar.LosSiguientes(1)
navegador.findElement(By.id("numeroidentificacion")).sendKeys(findTestData("DataForm").getValue("numero_documento",i))
navegador.findElement(By.id("nombres")).sendKeys(findTestData("DataForm").getValue("nombre_persona",i)) 
navegador.findElement(By.id("apellidos")).sendKeys(findTestData("DataForm").getValue("apellido_persona",i)) 
navegador.findElement(By.id("select2-chosen-17")).click()
navegador.findElement(By.id("s2id_autogen17_search")).sendKeys(findTestData("DataForm").getValue("genero",i) + Keys.ENTER) 
navegador.findElement(By.id("fechanacimiento")).sendKeys(findTestData("DataForm").getValue("fecha_nacimiento",i)) 
navegador.findElement(By.id("clave_uno")).sendKeys(findTestData("DataForm").getValue("clave",i)) 
navegador.findElement(By.id("clave_dos")).sendKeys(findTestData("DataForm").getValue("clave",i)) 
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
navegador.findElement(By.id("select2-chosen-19")).click()
navegador.findElement(By.id("s2id_autogen19_search")).sendKeys(findTestData("DataForm").getValue("municipio_contacto",i) + Keys.ENTER)
Esperar.LosSiguientes(2)
Seleccionar.dato(navegador.findElements(By.xpath("//*[@id='formulario_registro_direccionOcomuna']/label")), findTestData("DataForm").getValue("direccion",i))
Esperar.LosSiguientes(4)
navegador.findElement(By.id("select2-chosen-20")).click()
navegador.findElement(By.id("s2id_autogen20_search")).sendKeys(findTestData("DataForm").getValue("nombre_barrio",i) + Keys.ENTER)
Esperar.LosSiguientes(1)
navegador.findElement(By.id("s2id_formulario_registro_direccion_format_tipo_via")).click()
navegador.findElement(By.id("s2id_autogen22_search")).sendKeys(findTestData("DataForm").getValue("direccion_parte1",i) + Keys.ENTER)
Esperar.LosSiguientes(1)
navegador.findElement(By.id("formulario_registro_direccion_format_numero_via")).sendKeys(findTestData("DataForm").getValue("direccion_parte2",i))
navegador.findElement(By.id("s2id_formulario_registro_direccion_format_letra_prefijo")).click()
navegador.findElement(By.id("s2id_autogen23_search")).sendKeys(findTestData("DataForm").getValue("direccion_parte3",i) + Keys.ENTER)
navegador.findElement(By.id("s2id_formulario_registro_direccion_format_cuadrante")).click()
navegador.findElement(By.id("s2id_autogen25_search")).sendKeys(findTestData("DataForm").getValue("direccion_parte4",i) + Keys.ENTER)
navegador.findElement(By.id("formulario_registro_direccion_format_numero_generador")).sendKeys(findTestData("DataForm").getValue("direccion_parte5",i))
navegador.findElement(By.id("s2id_formulario_registro_direccion_format_letra_sufijo")).click()
navegador.findElement(By.id("s2id_autogen26_search")).sendKeys(findTestData("DataForm").getValue("direccion_parte6",i) + Keys.ENTER)
navegador.findElement(By.id("formulario_registro_direccion_format_numero_placa")).sendKeys(findTestData("DataForm").getValue("direccion_parte7",i))
navegador.findElement(By.id("s2id_formulario_registro_estrato")).click()
navegador.findElement(By.id("s2id_autogen28_search")).sendKeys(findTestData("DataForm").getValue("estrato",i) + Keys.ENTER)
//Seleccionar.dato(navegador.findElements(By.xpath("//*[@id='select2-results-28']/li")), '3')
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
navegador.findElement(By.id("correoelectronico")).click()
navegador.findElement(By.id("correoelectronico")).sendKeys(findTestData("DataForm").getValue("correo",i))
navegador.findElement(By.id("telefonomovil")).sendKeys(findTestData("DataForm").getValue("telefono",i))
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
Esperar.LosSiguientes(1)
MoverPagina.verticalmente(navegador, 100)
navegador.findElement(By.xpath("//*[@id='formatoAlto2']/div/div[3]/div/ins")).click()
navegador.findElement(By.xpath("//*[@id='formatoAlto2']/div/div[7]/div/ins")).click()
navegador.findElement(By.id("registro_save")).click()
Esperar.LosSiguientes(2)

if(navegador.findElement(By.xpath("/html/body/div[1]/div[2]/section/section/div[1]")).getText().contains(comprobacion)){
		System.out.println("Registro exitoso")
	}else{
		System.out.println("Error en la assertion...")
}

Esperar.LosSiguientes(5)
navegador.close()

}