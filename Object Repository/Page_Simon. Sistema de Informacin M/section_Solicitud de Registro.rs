<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Solicitud de Registro</name>
   <tag></tag>
   <elementGuidId>acd59312-5c61-40c4-9a88-29b1f59c2e4c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Volver al sitio web INDER Alcaldía de Medellín'])[1]/following::section[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>content container</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                        
                    
            
                
                    
                        Solicitud de Registro
                    
                    
                                            
                    
                        
                            
                            
                                Atención
                                Todos los campos marcados con * son obligatorios.                            
                        
                    
                    
                        
                            Tipo de persona
                               Seleccione una opción                             Persona NaturalOrganismos Deportivos
                        
                    
                
                
                
                    
                        
                            
                                
                            
                        
                        
                            1: Información general
                        
                    
                    
                        
                            
                                
                                    Tipo de documento
                                       Seleccione una opción                             Cédula de CiudadaníaCédula de ExtranjeríaNúmero de Identificación TributariaNumero único de Identificación PersonalPasaporteRegistro CivilTarjeta de Identidad
                                                                    
                                
                                    Número de identificación
                                    
                                                                        
                                
                            
                            
                                
                                    
                                        Nombre(s) completos
                                        
                                                                            
                                    
                                        Apellido(s) completos
                                        
                                                                            
                                
                                
                                    
                                        Sexo
                                           Seleccione una opción                             FemeninoMasculino
                                                                            
                                    
                                        Fecha de nacimiento
                                        
                                            
                                                
                                            
                                            

        

        if(typeof jQuery == 'undefined'){
            document.write('&lt;'+'script src=&quot;https://code.jquery.com/jquery-1.12.4.min.js&quot; integrity=&quot;sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=&quot; crossorigin=&quot;anonymous&quot;>&lt;'+'/script>');
        }

        document.addEventListener(&quot;DOMContentLoaded&quot;, function(event) {
            var scriptSrc = &quot;/bundles/itinputmask/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js&quot;;

            if ($('script[src=&quot;'+scriptSrc+'&quot;]').length &lt;= 0) {
                $.getScript(scriptSrc, function() {
                    Inputmask().mask(document.querySelectorAll(&quot;input&quot;));
                });
            }
        });
    


                                        
                                                                            
                                
                            
                            
                                
                                    
                                        Tipo de entidad
                                           Seleccione una opción                             Clubes de entidades no deportivasOtras organizaciones legalmente constituidasClub personas naturales
                                                                            
                                    
                                        Razón Social
                                        
                                                                            
                                
                            
                            
                                
                                    Clave
                                    
                                                                        
                                        Recomendaciones para su clave
                                        
                                            Debe contener al menos una letra.
                                            Debe contener al menos un número.
                                            Debe contener al menos 6 caracteres.
                                        
                                    
                                
                                
                                    Confirmar clave
                                    
                                                                    
                            
                        
                    
                
                
                
                    
                        
                            
                                
                            
                        
                        
                            2: Datos de contacto
                        
                    
                    
                        
                            
                                Municipio
                                   Seleccione una opción                             AbejorralAbregoAbriaquíAcacíasAcandíAcevedoAchíAguachicaAguadaAguadasAgua de DiosAguazulAgustín CodazziAipeAlbánAlbánAlbaniaAlbaniaAlbaniaAlcaláAldanaAlejandríaAlgarroboAlgecirasAlmaguerAlmeidaAlpujarraAltamiraAlto Baudó (Pie de Pató)Altos del RosarioAlvaradoAmagáAmalfiAmbalemaAnapoimaAncuyaAndalucíaAndesAngelópolisAngosturaAnolaimaAnoríAnsermaAnsermanuevoAnzáAnzoáteguiApartadóApíaApuloAquitaniaAracatacaAranzazuAratocaAraucaArauquitaArbeláezArboledaArboledasArboletesArcabucoArenalArgeliaArgeliaArgeliaAriguaníArjonaArmeniaArmeniaArmero GuayabalArroyohondoAstreaAtacoAyapelBagadóBahía SolanoBajo BaudóBalboaBalboaBaranoaBarayaBarbacoasBarbosaBarbosaBaricharaBarrancabermejaBarranca de UpíaBarrancasBarranco de LobaBarranco MinasBarranquillaBecerrilBelalcázarBelénBelénBelén de Los AndaquíesBelén de UmbríaBelloBelmiraBeltránBerbeoBetaniaBetéitivaBetuliaBetuliaBituimaBoavitaBochalemaBogotá D.C.BojacáBojayáBolívarBolívarBolívarBosconiaBoyacáBriceñoBriceñoBucaramangaBucarasicaBuenaventuraBuenavistaBuenavistaBuenavistaBuenavistaBuenos AiresBuesacoBugalagrandeBuriticáBusbanzáCabreraCabreraCabuyaroCacahualCáceresCachipayCachipayCáchiraCácotaCaicedoCaicedoniaCaimitoCajamarcaCajibíoCajicaCajicáCalamarCalamarCalarcáCaldasCaldasCaldonoCaliCaliforniaCalima EL DariénCalotoCampamentoCampoalegreCampo de la CruzCampohermosoCanaleteCañasgordasCandelariaCandelariaCantagalloCaparrapíCapitanejoCáquezaCaracolíCaramantaCarcasíCarepaCarmen de ApicaláCarmen de CarupaCarolina del PríncipeCartagena de IndiasCartagena del ChairáCartagoCarurúCasabiancaCastilla la NuevaCaucasiaCepitáCeretéCerinzaCerritoCerro de San AntonioCérteguiChachaguiChaguaníChalánChamezaChaparralCharaláChartaChíaChiboloChigorodóChimaChimáChimichaguaChinácotaChinavitaChinchináChinúChipaqueChipatáChiquinquiráChíquizaChiriguanáChiscasChitaChitagáChitaraqueChivatáChivorChoachíChocontáCicucoCiénagaCiénaga de OroCiénegaCimitarraCircasiaCisnerosCiudad BolívarClemenciaCocornáCoelloCoguaColombiaColónColón GenováColosóCómbitaConcepciónConcepciónConcordiaConcordiaCondotoConfinesConsacáContaderoContrataciónConvenciónCopacabanaCoperCórdobaCórdobaCórdobaCorintoCoromoroCorozalCorralesCotaCotorraCovarachíaCoveñasCoyaimaCravo NorteCuaspud CarlosamaCubaráCubarralCucaitaCucunubáCucutillaCuítivaCumaralCumariboCumbalCumbitaraCundayCurilloCuritíCurumaníDabeibaDaguaDibullaDistracciónDoloresDon MatíasDosquebradasDuitamaDuraníaEbéjicoEl AgradoEl ÁguilaEl AtratoEl BagreEl BancoEl CairoEl CalvarioEl Cantón de San PabloEl CarmenEl Carmen de AtratoEl Carmen de BolívarEl Carmen de ChucuríEl Carmen del DariénEl Carmen de ViboralEl CastilloEl CerritoEl CharcoEl CocuyEl ColegioEl CopeyEl DoncelloEl DoradoEl DovioEl EspinalEl EspinoEl GuacamayoEl GuamoEl GuamoElíasEl MolinoEl PasoEl PaujilEl PeñolEl PeñolEl PeñónEl PeñónEl PeñónEl PiñonEl PitalEl PlayónEl ReténEl RetiroEl RetornoEl RobleEl RosalEl RosarioEl SantuarioEl Tablón de GómezEl TamboEl TamboEl TarraEl ZuliaEncinoEncisoEntrerríosEnvigadoFacatativáFalanFiladelfiaFilandiaFiravitobaFlandesFlorenciaFlorenciaFlorestaFloriánFloridaFloridablancaFómequeFonsecaFortulFoscaFrancisco PizarroFredoniaFresnoFrontinoFuente de OroFundaciónFunesFunzaFúqueneFusagasugáGachaláGachancipáGachantiváGachetáGalánGalapaGalerasGamaGamarraGámbitaGámezaGaragoaGarzónGénovaGiganteGinebraGiraldoGirardotGirardotaGirónGómez PlataGonzálezGramaloteGranadaGranadaGranadaGuacaGuacamayasGuacaríGuachenéGuachetáGuachucalGuadalajara de BugaGuadalupeGuadalupeGuadalupeGuaduasGuaitarillaGualmatánGuamalGuamalGuapiGuapotáGuarandaGuarneGuascaGuatapéGuataquíGuatavitaGuatequeGuáticaGuavatáGuayabal de SíquimaGuayabetalGuayatáGüepsaGüicánGutiérrezHacaríHatillo de LobaHatoHato CorozalHatonuevoHeliconiaHerránHerveoHispaniaHoboHondaIbaguéIcononzoIlesImuésIníridaInzáIpialesIquiraIsnosIstminaItagüíItuangoIzaJambalóJamundíJardínJenesanoJericóJericóJerusalénJesús MaríaJordánJuan de AcostaJunínJuradoLa ApartadaLa ArgentinaLabatecaLa BellezaLabranzagrandeLa CaleraLa CapillaLa CejaLa CeliaLa ChorreraLa CruzLa CumbreLa DoradaLa EsperanzaLa EstrellaLa FloridaLa GloriaLa GuadalupeLa Jagua de IbiricoLa Jagua del PilarLa LlanadaLa MacarenaLa MercedLa MesaLa MontañitaLandázuriLa PalmaLa PazLa Paz RoblesLa PedreraLa PeñaLa PintadaLa PlataLa Playa De BelénLa PrimaveraLa SalinaLa SierraLa TebaidaLa TolaLa UniónLa UniónLa UniónLa UniónLa UvitaLa VegaLa VegaLa VictoriaLa VictoriaLa VictoriaLa VirginiaLebrijaLeivaLejaníasLenguazaqueLéridaLeticiaLíbanoLiborinaLinaresLitoral del San JuanLloróLópez De MicayLos Andes SotomayorLos CórdobasLos PalmitosLos PatiosLos SantosLourdesLuruacoMacanalMacaravitaMaceoMachetáMadridMaganguéMagui PayánMahatesMaicaoMajagualMálagaMalamboMallamaManatíManaureManaure Balcón del CesarManíManizalesMantaManzanaresMapiripánMapiripanaMargaritaMaría La BajaMarinillaMaripíMarmatoMarquetaliaMarsellaMarulandaMatanzaMedellínMedinaMedio AtratoMedio BaudóMedio San JuanMelgarMercaderesMesetasMilánMirafloresMirafloresMirandaMiriti(parana)MistratóMitúMocoaMogotesMolagavitaMomilMonguaMonguíMoniquiráMoñitosMontebelloMontecristoMontelíbanoMontenegroMonteríaMonterreyMoralesMoralesMoreliaMorichalMorroaMosqueraMosqueraMotavitaMurilloMurindóMutatáMutiscuaMuzoNariñoNariñoNariñoNátagaNatagaimaNechíNecoclíNeiraNeivaNemocónNiloNimaimaNobsaNocaimaNorcasiaNorosíNóvitaNueva GranadaNuevo ColónNunchíaNuquíObandoOcamonteOcañaOibaOicatáOlayaOlaya HerreraOnzagaOporapaOritoOrocuéOrtegaOspinaOtancheOvejasPachavitaPachoPacoaPácoraPadillaPáezPáez (Belalcázar)PaicolPailitasPaimePaipaPajaritoPalermoPalestinaPalestinaPalmarPalmar de VarelaPalmas del SocorroPalmiraPalocabildoPamplonaPamplonitaPana PanaPandiPanquebaPapunauaPáramoParatebuenoPascaPatíaPaunaPayaPaz de AriporoPaz de RíoPedrazaPelayaPensilvaniaPequePereiraPescaPiamontePiedecuestaPiedrasPiendamóPijaoPijiño del CarmenPinchotePinillosPiojoPisbaPitalitoPivijayPlanadasPlaneta RicaPlatoPolicarpaPolonuevoPonederaPopayánPorePotosíPraderaPradoProvidenciaProvidenciaPueblo BelloPueblo NuevoPueblo RicoPueblorricoPuebloviejoPuente NacionalPuerresPuerto AlegriaPuerto AricaPuerto AsísPuerto BerríoPuerto BoyacáPuerto CaicedoPuerto CarreñoPuerto ColombiaPuerto ColombiaPuerto ConcordiaPuerto EscondidoPuerto GaitánPuerto GuzmánPuerto LeguízamoPuerto LibertadorPuerto LlerasPuerto LópezPuerto NarePuerto NariñoPuerto NariñoPuerto ParraPuerto RicoPuerto RicoPuerto RondónPuerto SalgarPuerto SantanderPuerto SantanderPuerto TejadaPuerto TriunfoPuerto WilchesPulíPupialesPuracé (Coconuco)PurificaciónPurísimaQuebradanegraQuetameQuibdóQuimbayaQuinchíaQuípamaQuipileRagonvaliaRamiriquíRáquiraRecetorRegidorRemediosRemolinoRepelónRestrepoRestrepoRicaurteRicaurteRioblancoRío de OroRiofríoRiohachaRío IroRionegroRionegroRío QuitoRiosucioRiosucioRíoviejoRisaraldaRiveraRoberto Payán San JoséRoldanilloRoncesvallesRondónRosasRoviraSabana de TorresSabanagrandeSabanalargaSabanalargaSabanalargaSabanas de San ÁngelSabanetaSaboyáSácamaSáchicaSahagúnSaladoblancoSalaminaSalaminaSalazar De Las PalmasSaldañaSalentoSalgarSamacáSamanáSamaniegoSampuésSan AgustínSan AlbertoSan AndresSan AndrésSan Andrés de CuerquiaSan Andrés de SotaventoSan AnteroSan AntonioSan Antonio del TequendamaSan Antonio de PalmitoSan BenitoSan Benito AbadSan BernardoSan BernardoSan Bernardo del VientoSan CalixtoSan CarlosSan CarlosSan Carlos de GuaroaSan CayetanoSan CayetanoSan CristóbalSan DiegoSandonáSan EduardoSan EstanislaoSan FelipeSan FernandoSan FranciscoSan FranciscoSan FranciscoSan GilSan JacintoSan Jacinto del CaucaSan JerónimoSan JoaquínSan JoséSan José de CúcutaSan José de la MontañaSan José del FraguaSan José del GuaviareSan José del PalmarSan José de MirandaSan José de PareSan José de UréSan Juan de AramaSan Juan de BetuliaSan Juan del CesarSan Juan de PastoSan Juan de Río SecoSan Juan de UrabáSan JuanitoSan Juan NepomucenoSan LorenzoSan LuisSan LuisSan Luis de GacenoSan Luis de PalenqueSan MarcosSan MartínSan Martín de LobaSan Martín de los LlanosSan MateoSan MiguelSan MiguelSan Miguel de SemaSan OnofreSan PabloSan PabloSan Pablo de BorburSan PedroSan PedroSan Pedro de CartagoSan Pedro de los MilagrosSan Pedro de UrabáSan PelayoSan RafaelSan RoqueSan SebastiánSan Sebastián de BuenavistSan Sebastián De MariquitaSanta AnaSanta BárbaraSanta BárbaraSanta Bárbara de PintoSanta Bárbara - IscuandeSanta CatalinaSanta Cruz de LoricaSanta Cruz de MompósSantacruz GuachavésSanta Fe de AntioquiaSanta Helena del OpónSanta IsabelSanta LucíaSanta MaríaSanta MaríaSanta MartaSantanaSantander de QuilichaoSanta RosaSanta Rosa de CabalSanta Rosa Del NorteSanta Rosa del SurSanta Rosa de OsosSanta Rosa de ViterboSanta RosalíaSanta SofíaSantiagoSantiagoSantiago de TolúSanto DomingoSanto Domingo De SilosSanto TomásSantuarioSan Vicente de ChucuríSan Vicente del CaguánSan Vicente FerrerSan ZenónSapuyesSaravenaSardinataSasaimaSativanorteSativasurSegoviaSesquiléSevillaSiachoqueSibatéSibundoySilvaniaSilviaSimacotaSimijacaSimitíSincéSincelejoSipíSitionuevoSoachaSoatáSochaSocorroSocotáSogamosoSolanoSoledadSolitaSomondocoSonsónSopetránSoplavientoSopóSoraSoracáSotaquiráSotará (Paispamba)SuaitaSuanSuárezSuárezSuazaSubachoqueSucreSucreSucreSuescaSupatáSupíaSuratáSusaSusacónSutamarchánSutatausaSutatenzaTabioTadóTalaigua NuevoTamalamequeTámaraTameTámesisTaminangoTanguaTarairaTarapacaTarazáTarquiTarsoTascoTauramenaTausaTelloTenaTenerifeTenjoTenzaTeoramaTeruelTesaliaTibacuyTibanáTibasosaTibiritaTibúTierraltaTimanáTimbíoTimbiquíTinjacáTipacoqueTiquisioTitiribíTocaTocaimaTocancipáTogüíToledoToledoToluviejoTonaTópagaTopaipíToribioToroTotaTotoróTrinidadTrujilloTubaráTuchínTuluáTumacoTunjaTununguáTúquerresTurbacoTurbanaTurboTurmequéTutaTutazáUbaláUbaqueUbatéUlloaUmbitaUneUnguíaUnión PanamericanaUramitaUribeUribiaUrraoUrumitaUsiacuríÚticaValdiviaValenciaValle del GuamuezValle de San JoséValle de San JuanValleduparValparaísoValparaísoVegachíVélezVenadilloVeneciaVeneciaVentaquemadaVergaraVersallesVetasVianíVictoriaVigía del FuerteVijesVilla CaroVilla de LeyvaVilla del RosarioVillagarzónVillagómezVillahermosaVillamaríaVillanuevaVillanuevaVillanuevaVillanuevaVillapinzónVilla RicaVillarricaVillavicencioVillaviejaVilletaViotáViracacháVistahermosaViterboYacopíYacuanquerYaguaráYaliYarumalYavarateYolombóYondóYopalYotocoYumboZambranoZapatocaZapayánZaragozaZarzalZetaquiráZipacónZipaquiráZona Bananera
                                                            
                            
                                Dirección
                                VeredaBarrio
                                                            
                            
                                Barrio
                                   Seleccione una opción   Barrio          Barrio                
                                                            
                            
                                
                                    Comuna o corregimiento
                                        
        Por favor ingrese la dirección utilizando cada uno de los campos.
    
            
        
            
                
                       Ninguna opción                             Ninguna opciónKilometroCorregimiento
                                    
                
                    
                                    
                
                    
                                    
            
        
    


                                                                    
                                
                                    Confirma aquí la dirección
                                    
                                                                    
                            
                            
                                
                                    Dirección
                                        
                Por favor ingrese la dirección utilizando cada uno de los campos.
    
            
        
            
                
                    
                        Esta guía te puede ayudar a ingresar tu dirección. Utiliza los campos que necesites.
                        
                    
                
                
                       Ninguna opción                             Ninguna opciónAutopistaAvenidaCalleCarreraDiagonalTransversal
                                    
                
                    
                                    
                
                       Ninguna opción                             Ninguna opciónABCDEFGHIJKLMNOPQRSTUVWXYZ
                                    
                
                       Ninguna opción                             Ninguna opciónBIS
                                    
                
                       Ninguna opción                             Ninguna opciónNorteOesteOrienteSur
                                    
                
                    
                                    
                
                       Ninguna opción                             Ninguna opciónABCDEFGHIJKLMNOPQRSTUVWXYZ
                                    
                
                       Ninguna opción                             Ninguna opciónBIS
                                    
                
                    
                                    
            
        
    


                                                                    
                                
                                    Confirma aquí la dirección
                                    
                                                                    
                            
                            
                                Estrato
                                   Seleccione una opción   Estrato          Estrato                 123456
                                                            
                            
                                Correo electrónico
                                
                                                            
                            
                                Teléfono móvil / Teléfono fijo
                                
                                                            
                        
                    
                
                       
                
                    
                        
                            
                                
                            
                        
                        
                            3: Política de protección de datos, Términos y condiciones
                        
                    
                    
                        
                            
                                El INDER-MEDELLÍN, Se encuentra comprometido con el adecuado manejo de los datos personales y en calidad de responsable dando cumplimiento a la Ley 1581 de 2012 y el decreto 1074 de 2015 de Protección de Datos Personales, por ello entiende que la persona al insertar sus datos personales en el presente documento ha leído, entendido y aceptado la política de protección de datos aceptando la finalidad dada a los datos aquí recolectados para la realización de capacitaciones, desarrollo de estrategias INDER, listados de asistencia, formación, y cualquier otra actividad liderada por el instituto, dispuesta en la política de la institución. SE ENTIENDE QUE LA AUTORIZACIÓN ES EXPRESA E INFORMADA por parte del titular de datos en el presente documento de registro. Cualquier inquietud favor dirigirse al correo institucional proteccion.datos@inder.gov.co o consulte los demás canales de atención en la política divulgada en https://www.inder.gov.co para ejercer los derechos de conocer, actualizar, rectificar, suprimir y revocar la autorización.
                            
                            
                                
                                    
                                        
                                        Descarga nuestra política de protección de datos
                                    
                                
                            
                            
                                Aceptar políticas de Habeas Data 
                                
                            
                            
                                
                            
                            
                                
                            
                            
                                
                                    
                                        
                                        Conoce nuestros Términos y condiciones
                                    
                                
                            
                            
                                Aceptar Términos y condiciones *
                                
                                                            
                        
                    
                
                
                
                    
                        
                            
                            Guardar                        
                        
                            
                            Cancelar                        
                    
                
            
            

            
            
    
                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;sonata-bc skin-black-light layout-top-nav&quot;]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-wrapper content-wrapper-ajuste&quot;]/section[@class=&quot;content container&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Volver al sitio web INDER Alcaldía de Medellín'])[1]/following::section[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Toggle navigation'])[1]/following::section[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//section</value>
   </webElementXpaths>
</WebElementEntity>
