**Automatización web bajo el framework Katalon**

Para este reto se usa la herramienta Selenium dentro del framework Katalon, el cual, permite que sea usado código Java para desarrollar la automatización básica solicitada.

*Se debe tener en cuenta que se usa un archivo llamada data.xlsx el cual representa el datadriven o el set de datos que serán cargado en la automatización.*

---

## Clases generadas

Se crean clases externas para que éstas sean reutilizadas en la codificación y así se reduzca la cantidad de líneas empleadas en el mismo.

---

## Datadriven Excel

Se logra implementar un pequeño set de datos cargado desde un archivo de Excel para evitar quemar en el código los datos que serán ejecutados en el formulario de registro.

---